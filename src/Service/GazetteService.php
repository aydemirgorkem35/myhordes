<?php


namespace App\Service;

use App\Entity\CauseOfDeath;
use App\Entity\Citizen;
use App\Entity\Gazette;
use App\Entity\GazetteEntryTemplate;
use App\Entity\GazetteLogEntry;
use App\Entity\Town;
use App\Entity\Zone;
use App\Translation\T;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

class GazetteService
{
    private EntityManagerInterface $entity_manager;
    private LogTemplateHandler $log;
    private TranslatorInterface $translator;
    private RandomGenerator $rand;

    public function __construct(
        EntityManagerInterface $em, LogTemplateHandler $lh, TranslatorInterface $translator, RandomGenerator $rand)
    {
        $this->entity_manager = $em;
        $this->log = $lh;
        $this->translator = $translator;
        $this->rand = $rand;
    }

    public function check_gazettes(Town $town) {
        $need = [ $town->getDay() => true, $town->getDay() + 1 => true, $town->getDay() + 2 => true ];

        foreach ($town->getGazettes() as $gazette)
            if (isset($need[$gazette->getDay()])) $need[$gazette->getDay()] = false;

        foreach ($need as $day => $create)
            if ($create) $town->addGazette((new Gazette())->setDay($day));

    }

    protected function parseLog( $template, array $variables, $lowercase = false ): String {
        $variableTypes = $template->getVariableTypes();
        $transParams = $this->log->parseTransParams($variableTypes, $variables);

        try {
            if ($lowercase) {
                $proto = $this->translator->trans($template->getText(), [], 'game');
                $lowercase = (substr($proto, 0, 1) !== strtolower(substr($proto, 0, 1)));
            }
            $text = $this->translator->trans($template->getText(), $transParams, 'game');
            if ($lowercase)
                $text = strtolower(substr($text, 0, 1)) . substr($text, 1);
        }
        catch (Exception $e) {
            $text = "null";
        }

        return $text;
    }

    public function parseGazetteLog(GazetteLogEntry $gazetteLogEntry, $lowercase = false): string
    {
        $flavour = [
            GazetteEntryTemplate::FollowUpTypeDoubt => [
                null,
                $this->translator->trans('Nun, das sage ich...', [],'game'),
                $this->translator->trans('Das ist doch Unsinn...', [], 'game'),
                $this->translator->trans('Die einen sagen dies, die anderen das...', [], 'game'),
                $this->translator->trans('Ich sags ja nur...', [], 'game'),
                $this->translator->trans('Komm schon, glaub mir.', [], 'game'),
            ],
            GazetteEntryTemplate::FollowUpTypeBad => [
                null,
                $this->translator->trans('Was für eine Organisation...', [], 'game'),
                $this->translator->trans('Wir werden nicht lange durchhalten, das sage ich euch.', [], 'game'),
                $this->translator->trans('Noch so eine Nacht und wir werden nicht mehr hier sein, um über so etwas zu reden.', [], 'game'),
                $this->translator->trans('Das war knapp.', [], 'game'),
                $this->translator->trans('Pfff...', [], 'game'),
            ],
        ];
        $txt = $this->parseLog($gazetteLogEntry->getTemplate(), $gazetteLogEntry->getVariables(), $lowercase);

        if ($txt && $gazetteLogEntry->getTemplate() && $gazetteLogEntry->getFollowUp() > 0 &&
            isset($flavour[ $gazetteLogEntry->getTemplate()->getFollowUpType() ]) && isset($flavour[ $gazetteLogEntry->getTemplate()->getFollowUpType() ][ $gazetteLogEntry->getFollowUp() ]))

            $txt .= " {$flavour[ $gazetteLogEntry->getTemplate()->getFollowUpType() ][ $gazetteLogEntry->getFollowUp() ]}";
        return $txt;
    }

    protected function generate_wind_entry(Gazette $gazette, int $day): string {
        if ($gazette->getWindDirection() === 0) return '';
        $townTemplate = $this->rand->pick( $this->entity_manager->getRepository(GazetteEntryTemplate::class)->findBy(['type' => GazetteEntryTemplate::TypeGazetteWind]) );
        if ($townTemplate === null) return '';

        $variables = [];

        switch ($gazette->getWindDirection()) {
            case Zone::DirectionNorthWest:
                $variables['sector'] = T::__('Nordwesten', 'game');
                $variables['sector2'] = T::__('im Nordwesten', 'game');
                break;
            case Zone::DirectionNorth:
                $variables['sector'] = T::__('Norden', 'game');
                $variables['sector2'] = T::__('im Norden', 'game');
                break;
            case Zone::DirectionNorthEast:
                $variables['sector'] = T::__('Nordosten', 'game');
                $variables['sector2'] = T::__('im Nordosten', 'game');
                break;
            case Zone::DirectionWest:
                $variables['sector'] = T::__('Westen', 'game');
                $variables['sector2'] = T::__('im Westen', 'game');
                break;
            case Zone::DirectionEast:
                $variables['sector'] = T::__('Osten', 'game');
                $variables['sector2'] = T::__('im Osten', 'game');
                break;
            case Zone::DirectionSouthWest:
                $variables['sector'] = T::__('Südwesten', 'game');
                $variables['sector2'] = T::__('im Südwesten', 'game');
                break;
            case Zone::DirectionSouth:
                $variables['sector'] = T::__('Süden', 'game');
                $variables['sector2'] = T::__('im Süden', 'game');
                break;
            case Zone::DirectionSouthEast:
                $variables['sector'] = T::__('Südosten', 'game');
                $variables['sector2'] = T::__('im Südosten', 'game');
                break;
        }
        $news = new GazetteLogEntry();
        $news->setDay($day)->setGazette($gazette)->setTemplate($townTemplate)->setVariables($variables);
        $this->entity_manager->persist($news);
        return $this->parseGazetteLog($news);
    }

    protected function decompose_requirements( GazetteEntryTemplate $g ): array {

        $base = ($g->getRequirement() < 100) ? $g->getRequirement() * 10 : $g->getRequirement();
        $class = intval(floor($base / 100));
        $arg1 = intval(floor(($base-$class*100) / 10));
        $arg2 = intval($base-$class*100-$arg1*10);

        switch ($class) {
            case 1:case 2:case 3:case 5:
                return [ $class * 10, $arg1 ];
            case 4:case 6:
                return [ $class*10 + $arg1, $arg2 ];
            default:
                return [0,0];
        }
    }

    /**
     * @param Gazette $gazette
     * @param array $criteria
     * @param Citizen[] $survivors
     * @param Citizen[] $death_inside
     * @param Citizen[] $death_outside
     * @return GazetteEntryTemplate|null
     */
    protected function select_gazette_template( Gazette $gazette, array $criteria, array $survivors = [], array $death_inside = [], array $death_outside = [] ): ?GazetteEntryTemplate {
        $templates = array_filter( $this->entity_manager->getRepository(GazetteEntryTemplate::class)->findBy($criteria), function(GazetteEntryTemplate $g) use ($gazette,$survivors,$death_inside,$death_outside) {
            list($class,$arg) = $this->decompose_requirements( $g );
            switch ( $class ) {
                case GazetteEntryTemplate::BaseRequirementCitizen:        return count( $survivors )    >= $arg;
                case GazetteEntryTemplate::BaseRequirementCadaver:        return count( $death_inside ) >= $arg;
                case GazetteEntryTemplate::BaseRequirementCitizenCadaver: return count( $survivors )    >= $arg && count( $death_inside ) >= $arg;
                case GazetteEntryTemplate::BaseRequirementCitizenInTown:  return count( array_filter( $survivors, fn(Citizen $c) => $c->getZone() === null ) ) >= $arg;
                case GazetteEntryTemplate::RequiresMultipleDehydrations:  return count( $survivors ) >= $arg && count( array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Dehydration ) ) >= 2;
                case GazetteEntryTemplate::RequiresMultipleSuicides:      return count( $survivors ) >= $arg && count( array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Cyanide ) ) >= 2;
                case GazetteEntryTemplate::RequiresMultipleInfections:    return count( $survivors ) >= $arg && count( array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Infection ) ) >= 2;
                case GazetteEntryTemplate::RequiresMultipleVanished:      return count( $survivors ) >= $arg && count( array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Vanished ) ) >= 2;
                case GazetteEntryTemplate::RequiresMultipleHangings:      return count( $survivors ) >= $arg && count( array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Hanging ) ) >= 2;
                case GazetteEntryTemplate::RequiresMultipleCrosses:       return count( $survivors ) >= $arg && count( array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::ChocolateCross ) ) >= 2;
                case GazetteEntryTemplate::RequiresMultipleRedSouls:      return count( $survivors ) >= $arg && count( array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Haunted ) ) >= 2;
                case GazetteEntryTemplate::RequiresInvasion:              return $gazette->getInvasion() > 0;
                case GazetteEntryTemplate::RequiresAttackDeaths:          return count( $survivors ) >= $arg && $gazette->getDeaths() > 0;
                default: return true;
            }
        });

        return $this->rand->pick( $templates );
    }

    /**
     * @param Gazette $gazette
     * @param GazetteEntryTemplate $g
     * @param Citizen[] $survivors
     * @param Citizen[] $death_inside
     * @param Citizen[] $death_outside
     * @param Citizen|null $featured
     * @return array
     */
    protected function fill_template_variables( Gazette $gazette, GazetteEntryTemplate $g, array $survivors, array $death_inside, array $death_outside, ?Citizen $featured = null ): array {
        $variables = [];

        list($class,$arg) = $this->decompose_requirements( $g );

        $_add_elements = function( $name, array $list, ?Citizen $featured, $n, &$var ) {
            $elems = array_filter( $list, fn(Citizen $c) => $c !== $featured );
            if ($n > 0) shuffle($elems);
            if ($featured !== null) array_unshift( $elems, $featured );

            for ($i = 1; $i <= $n; $i++)
                $var["$name$i"] = (array_shift($elems))->getId();
            $var["{$name}s"] = count($elems);
        };

        foreach ($g->getVariableTypes() as ['name' => $name])
            switch ($name) {
                case 'poison':
                    $variables['poison'] = $this->rand->pick( [
                        T::__('Arsen','game'),T::__('Cyanolin','game'),T::__('Neurotwinin','game'),
                        T::__('Rizin','game'),T::__('Botulinumtoxin','game'),T::__('Virunoir','game'),
                        T::__('Chlorotwinat','game'),T::__('Kaliumchlorid','game'),T::__('Kurare','game'),
                        T::__('Zyanid (poison)','game'),T::__('Phenol','game'),T::__('Ponasulfat','game')
                    ] );
                    break;
                case 'location':
                    $variables['location'] = $this->rand->pick( [
                        T::__('in der Nähe des Gemüsegartens','game'),T::__('in der Nähe der südlichen Stadtmauer','game'),
                        T::__('am Stadttor','game'),T::__('am Aussichtsturm','game'),
                        T::__('am westlichen Flügel der Stadt','game'),T::__('an der nördlichen Stadtmauer','game'),
                        T::__('an unserer schwach gesicherten Flanke','game'),T::__('in der Nähe des Brunnens','game'),
                        T::__('an der östlichen Stadtmauer','game'),T::__('in der Stadtmauer am Stadttor','game'),
                    ] );
                    break;
                case 'mascot':
                    $variables['mascot'] = $this->rand->pick( [
                        T::__('Drops','game'),T::__('Whitie','game'),T::__('Fettwanst','game'),
                        T::__('Gizmo','game'),T::__('Romero','game'),T::__('Krümel','game'),
                        T::__('Trolli','game'),T::__('Warpie','game'),T::__('Panda','game'),
                        T::__('Urmel','game'),T::__('Nuffi','game'),T::__('Kampfzwerg','game'),
                        T::__('Brummbrumm','game'),T::__('Quälgeist','game'),T::__('Wuschel','game'),
                    ] );
                    break;
                case 'animal':
                    $variables['animal'] = $this->rand->pick( [
                        T::__('Ratte','game'),T::__('Ziege','game'),T::__('Pudel','game'),
                        T::__('Hamster','game'),T::__('Köter','game'),T::__('Schwein','game'),
                        T::__('Erdmännchen','game'),T::__('Wasserschwein','game'),T::__('Sumpfschildkröte','game'),
                        T::__('Kätzchen','game'),
                    ] );
                    break;
                case 'item':
                    $variables['item'] = $this->rand->pick( [
                        T::__('einen Haufen Gerümpel ','game'),T::__('einen Holzstapel ','game'),
                        T::__('einen Haufen Schrott','game'),T::__('einen toter Baumstamm','game'),
                        T::__('eine vergessene Leiter','game'),T::__('einen Berg von Kisten','game'),
                        T::__('einen Turm aus Trümmern','game'),T::__('einen Haufen Plunder','game'),
                        T::__('einen Steinhaufen','game'),T::__('einen Haufen Ramsch','game'),
                    ] );
                    break;
                case 'random':
                    $variables['random'] = mt_rand(15,99);
                    break;
                case 'randomHour':
                    $variables['randomHour'] = mt_rand(0,12);
                    break;
            }

        switch ($class) {
            case GazetteEntryTemplate::BaseRequirementCitizen:
                $_add_elements( 'citizen', $survivors, $featured !== null && $featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::BaseRequirementCadaver:
                $_add_elements( 'cadaver', $death_inside, $featured !== null && !$featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::BaseRequirementCitizenCadaver:
                $_add_elements( 'citizen', $survivors, $featured !== null && $featured->getAlive() ? $featured : null, $arg, $variables );
                $_add_elements( 'cadaver', $death_inside, $featured !== null && !$featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::RequiresAttack:
                $attack = $gazette->getAttack();
                $variables['attack'] = $attack < 2000 ? 10 * (round($attack / 10)) : 100 * (round($attack / 100));
                break;

            case GazetteEntryTemplate::RequiresInvasion:
                $attack = $gazette->getAttack();
                $variables['attack'] = $attack < 2000 ? 10 * (round($attack / 10)) : 100 * (round($attack / 100));
                $variables['invasion'] = $gazette->getInvasion();
                break;

            case GazetteEntryTemplate::RequiresAttackDeaths:
                $attack = $gazette->getAttack();
                $variables['attack'] = $attack < 2000 ? 10 * (round($attack / 10)) : 100 * (round($attack / 100));
                $variables['deaths'] = $variables['cadavers'] = $gazette->getDeaths();
                if ($arg > 0) $_add_elements( 'citizen', $survivors, $featured !== null && $featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::RequiresDefense:
                $defense = $gazette->getDefense();
                $variables['defense'] = $defense < 2000 ? 10 * (round($defense / 10)) : 100 * (round($defense / 100));
                break;

            case GazetteEntryTemplate::RequiresDeaths:
                $variables['deaths'] = $variables['cadavers'] = $gazette->getDeaths();
                break;

            case GazetteEntryTemplate::RequiresMultipleDehydrations:
                $_add_elements('cadaver', array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Dehydration ), null, 0, $variables);
                if ($arg > 0) $_add_elements( 'citizen', $survivors, $featured !== null && $featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::RequiresMultipleSuicides:
                $_add_elements('cadaver', array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Cyanide ), null, 0, $variables);
                if ($arg > 0) $_add_elements( 'citizen', $survivors, $featured !== null && $featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::RequiresMultipleInfections:
                $_add_elements('cadaver', array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Infection ), null, 0, $variables);
                if ($arg > 0) $_add_elements( 'citizen', $survivors, $featured !== null && $featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::RequiresMultipleVanished:
                $_add_elements('cadaver', array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Vanished ), null, 0, $variables);
                if ($arg > 0) $_add_elements( 'citizen', $survivors, $featured !== null && $featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::RequiresMultipleHangings:
                $_add_elements('cadaver', array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Hanging ), null, 0, $variables);
                if ($arg > 0) $_add_elements( 'citizen', $survivors, $featured !== null && $featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::RequiresMultipleCrosses:
                $_add_elements('cadaver', array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::ChocolateCross ), null, 0, $variables);
                if ($arg > 0) $_add_elements( 'citizen', $survivors, $featured !== null && $featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::RequiresMultipleRedSouls:
                $_add_elements('cadaver', array_filter( $death_outside, fn(Citizen $c) => $c->getCauseOfDeath()->getRef() === CauseOfDeath::Haunted ), null, 0, $variables);
                if ($arg > 0) $_add_elements( 'citizen', $survivors, $featured !== null && $featured->getAlive() ? $featured : null, $arg, $variables );
                break;

            case GazetteEntryTemplate::BaseRequirementCitizenInTown:
                $citizens = array_filter( $survivors, fn(Citizen $c) => $c->getZone() === null && $c !== $featured );
                shuffle($citizens);
                if ($featured !== null && $featured->getAlive() && $featured->getZone() === null) array_unshift( $citizens, $featured );

                for ($i = 1; $i <= $arg; $i++)
                    $variables['citizen' . $i] = (array_shift($citizens))->getId();
                $variables['citizens'] = count($citizens);

                break;
        }

        return $variables;
    }

    protected function enrichLogEntry( GazetteLogEntry $g ): GazetteLogEntry {
        if (!$g->getTemplate()) return $g;
        if ($g->getTemplate()->getFollowUpType() !== 0)
            $g->setFollowUp( max(0, mt_rand(-9,5)) );
        return $g;
    }

    /**
     * @param Gazette $gazette
     * @param Town $town
     * @param int $day
     * @param Citizen[] $survivors
     * @param Citizen[] $death_inside
     * @return bool
     */
    protected function gazette_section_town( Gazette $gazette, Town $town, int $day, array $survivors = [], array $death_inside = [] ): bool {
        if ( $gazette->getReactorExplosion() ) $criteria = [ 'type' => GazetteEntryTemplate::TypeGazetteReactor ];
        elseif ( $town->getDevastated() ) $criteria = [ 'name' => 'gazetteTownDevastated' ];
        elseif (count($death_inside) > 0 && count($death_inside) < 5 && $gazette->getDoor()) $criteria = [ 'type' => GazetteEntryTemplate::TypeGazetteDeathWithDoorOpen ];
        elseif ($gazette->getDay() === 2 && count($death_inside) === 0) $criteria = [ 'type' => GazetteEntryTemplate::TypeGazetteDayOne ];
        else $criteria = [ 'type' => GazetteEntryTemplate::TypeGazetteNoDeaths + (min(count($death_inside), 3)) ];

        if ($townTemplate = $this->select_gazette_template($gazette, $criteria, $survivors, $death_inside)) {
            $variables = $this->fill_template_variables($gazette, $townTemplate, $survivors, $death_inside, []);
            $this->entity_manager->persist( $this->enrichLogEntry( (new GazetteLogEntry())->setDay($day)->setGazette($gazette)->setTemplate($townTemplate)->setVariables($variables) ) );
            return true;
        } else return false;
    }

    /**
     * @param Gazette $gazette
     * @param Town $town
     * @param int $day
     * @param Citizen[] $survivors
     * @param Citizen[] $death_inside
     * @param Citizen[] $death_outside
     * @return bool
     */
    protected function gazette_section_individual_deaths( Gazette $gazette, Town $town, int $day, array $survivors = [], array $death_inside = [], array $death_outside = [] ): bool {
        if (!$town->getDevastated() && !$gazette->getReactorExplosion() && count($death_outside) > 0) {

            $type = null; $featured_cadaver = null;

            // Check for multi deaths
            $d_list = [];
            foreach ($death_outside as $citizen)
                if ($citizen->getCauseOfDeath() && !isset($d_list[$citizen->getCauseOfDeath()->getRef()])) $d_list[$citizen->getCauseOfDeath()->getRef()] = 1;
                else $d_list[$citizen->getCauseOfDeath()->getRef()]++;

            $d_list = array_filter( $d_list, fn($v,$k) => $v >= 2 && in_array($k,[CauseOfDeath::Cyanide,CauseOfDeath::Dehydration,CauseOfDeath::Infection,CauseOfDeath::Vanished,CauseOfDeath::Hanging,CauseOfDeath::ChocolateCross,CauseOfDeath::Haunted]), ARRAY_FILTER_USE_BOTH );
            $focus = $this->rand->pick( array_keys($d_list) );

            if ($focus !== null) switch ($focus) {
                case CauseOfDeath::Cyanide:
                    $type = GazetteEntryTemplate::TypeGazetteMultiSuicide; break;
                case CauseOfDeath::Dehydration:
                    $type = GazetteEntryTemplate::TypeGazetteMultiDehydration; break;
                case CauseOfDeath::Infection:
                    $type = GazetteEntryTemplate::TypeGazetteMultiInfection; break;
                case CauseOfDeath::Vanished:
                    $type = GazetteEntryTemplate::TypeGazetteMultiVanished; break;
                case CauseOfDeath::Hanging:
                    $type = GazetteEntryTemplate::TypeGazetteMultiHanging; break;
                case CauseOfDeath::ChocolateCross:
                    $type = GazetteEntryTemplate::TypeGazetteMultiChocolateCross; break;
                case CauseOfDeath::Haunted:
                    $type = GazetteEntryTemplate::RequiresMultipleRedSouls; break;
            }

            // Check for individual deaths
            if ($type === null) {
                $featured_cadaver = $this->rand->pick( array_filter( $death_outside, fn(Citizen $c) => in_array($c->getCauseOfDeath()->getRef(), [
                    CauseOfDeath::Cyanide,
                    CauseOfDeath::Strangulation,
                    CauseOfDeath::Addiction,
                    CauseOfDeath::Dehydration,
                    CauseOfDeath::Poison,
                    CauseOfDeath::Haunted,
                    CauseOfDeath::Infection,
                    CauseOfDeath::Vanished,
                    CauseOfDeath::Hanging,
                    CauseOfDeath::ChocolateCross
                ]) ) );

                if ($featured_cadaver === null)
                    return false;

                switch ($featured_cadaver->getCauseOfDeath()->getRef()) {
                    case CauseOfDeath::Cyanide:
                    case CauseOfDeath::Strangulation:
                        $type = GazetteEntryTemplate::TypeGazetteSuicide;
                        break;

                    case CauseOfDeath::Addiction:
                        $type = GazetteEntryTemplate::TypeGazetteAddiction;
                        break;

                    case CauseOfDeath::Dehydration:
                        $type = GazetteEntryTemplate::TypeGazetteDehydration;
                        break;

                    case CauseOfDeath::Poison:
                        $type = GazetteEntryTemplate::TypeGazettePoison;
                        break;

                    case CauseOfDeath::Haunted:
                        $type = GazetteEntryTemplate::TypeGazetteRedSoul;
                        break;

                    case CauseOfDeath::Infection:
                        $type = GazetteEntryTemplate::TypeGazetteInfection;
                        break;

                    case CauseOfDeath::Vanished:
                        $type = GazetteEntryTemplate::TypeGazetteVanished;
                        break;
                    case CauseOfDeath::Hanging:
                        $type = GazetteEntryTemplate::TypeGazetteHanging;
                        break;
                    case CauseOfDeath::ChocolateCross:
                        $type = GazetteEntryTemplate::TypeGazetteChocolateCross;
                        break;
                }
            }

            if ($type === null) return false;

            $criteria = [ 'type' => $type ];

            if ($townTemplate = $this->select_gazette_template($gazette, $criteria, $survivors, $death_outside, $death_outside)) {
                $variables = $this->fill_template_variables($gazette, $townTemplate, $survivors, $death_outside, $death_outside, $featured_cadaver);
                $this->entity_manager->persist( $this->enrichLogEntry( (new GazetteLogEntry())->setDay($day)->setGazette($gazette)->setTemplate($townTemplate)->setVariables($variables) ) );
                return true;
            } else return false;

        } else return false;
    }

    /**
     * @param Gazette $gazette
     * @param Town $town
     * @param int $day
     * @param Citizen[] $survivors
     * @param Citizen[] $death_inside
     * @param Citizen[] $death_outside
     * @return bool
     */
    protected function gazette_section_role_deaths( Gazette $gazette, Town $town, int $day, array $survivors = [], array $death_inside = [], array $death_outside = [] ): bool {
        if (!$town->getDevastated() && !$gazette->getReactorExplosion() && count($death_outside) > 0) {

            $type = null;

            $shaman = $guide = $cata = $ghoul = false;
            foreach ($gazette->getVotesNeeded() as $role) {
                $shaman = $shaman || $role->getName() === 'shaman';
                $guide  = $guide  || $role->getName() === 'guide';
                $cata   = $cata   || $role->getName() === 'cata';
                $ghoul  = $ghoul  || $role->getName() === 'ghoul';
            }

            if ($shaman && $guide) $type = GazetteEntryTemplate::TypeGazetteGuideShamanDeath;
            elseif ($shaman)       $type = GazetteEntryTemplate::TypeGazetteShamanDeath;
            elseif ($guide)        $type = GazetteEntryTemplate::TypeGazetteGuideDeath;

            if ($type === null) return false;

            $criteria = [ 'type' => $type ];

            if ($townTemplate = $this->select_gazette_template($gazette, $criteria, $survivors, $death_inside, $death_outside)) {
                $variables = $this->fill_template_variables($gazette, $townTemplate, $survivors, $death_inside, $death_outside);
                $this->entity_manager->persist( $this->enrichLogEntry( (new GazetteLogEntry())->setDay($day)->setGazette($gazette)->setTemplate($townTemplate)->setVariables($variables) ) );
                return true;
            } else return false;
        } else return false;
    }

    protected function gazette_section_flavour( Gazette $gazette, Town $town, int $day, array $survivors = [], array $death_inside = [] ): bool {
        if ( $gazette->getReactorExplosion() || $town->getDevastated() || $gazette->getDeaths() > 0 ) return false;

        $criteria = [ 'type' => GazetteEntryTemplate::TypeGazetteFlavour ];

        if ($townTemplate = $this->select_gazette_template($gazette, $criteria, $survivors, $death_inside)) {
            $variables = $this->fill_template_variables($gazette, $townTemplate, $survivors, $death_inside, []);
            $this->entity_manager->persist( $this->enrichLogEntry( (new GazetteLogEntry())->setDay($day)->setGazette($gazette)->setTemplate($townTemplate)->setVariables($variables) ) );
            return true;
        } else return false;
    }

    public function ensureGazette(Town $town, ?int $day = null, ?bool &$created = null): ?Gazette {
        $day = min( $town->getDay(), $day ?? $town->getDay() );
        $gazette = $town->findGazette( $day, true );

        /** @var GazetteLogEntry[] $gazette_logs */
        $gazette_logs = $this->entity_manager->getRepository(GazetteLogEntry::class)->findBy(['gazette' => $gazette]);
        $wind = "";

        $created = false;
        if (count($gazette_logs) == 0) {
            $created = true;
            $death_inside = $gazette->getVictimBasedOnLocation(true);
            $death_outside = $gazette->getVictimBasedOnLocation(false);

            if (count($death_inside) > $gazette->getDeaths()) {
                $gazette->setDeaths(count($death_inside));
            }

            if ($day > 1)  {
                $survivors = [];
                foreach ($town->getCitizens() as $citizen) {
                    if(!$citizen->getAlive()) continue;
                    $survivors[] = $citizen;
                }

                $sections = 0;

                // 1. TOWN
                if ($this->gazette_section_town( $gazette, $town, $day, $survivors, $death_inside ))
                    $sections++;

                // 2. DEATHS
                if ($this->gazette_section_individual_deaths( $gazette, $town, $day, $survivors, $death_inside, $death_outside ))
                    $sections++;

                // 3. FLAVOURS
                if ($sections < 2 && $this->rand->chance(0.2))
                    $this->gazette_section_flavour( $gazette, $town, $day, $survivors, $death_inside );

                // 4. ELECTION
                $this->gazette_section_role_deaths( $gazette, $town, $day, $survivors, $death_inside, $death_outside );

                // 5. SEARCH TOWER
                $wind = $this->generate_wind_entry($gazette,$day);
            }
        }

        // Ensure the town has a wind direction log if applicable
        if (empty($wind) && $gazette->getWindDirection() !== 0)
            $this->generate_wind_entry($gazette,$day);

        return $gazette;
    }

    public function renderGazette( Town $town, ?int $day = null, bool $allow_dynamic_creation = false, ?string $lang = null ): array {
        $origLang = $this->translator->getLocale();
        if($lang !== null) {
            $this->translator->setLocale($lang);
        }

        $day = min( $town->getDay(), $day ?? $town->getDay() );
        $death_outside = $death_inside = [];

        /** @var Gazette $gazette */
        $gazette = $this->ensureGazette($town, $day, $created);

        if ($created && $allow_dynamic_creation)
            $this->entity_manager->flush();

        foreach ($gazette->getVictims() as $citizen) {
            if ($citizen->getAlive()) continue;
            if ($citizen->getCauseOfDeath()->getRef() == CauseOfDeath::NightlyAttack)
                $death_inside[] = $citizen;
            else
                $death_outside[] = $citizen;
        }

        $text = '';
        $wind = '';

        if ($day === 1) {
            $text = "<p>" . $this->translator->trans('Heute Morgen ist kein Artikel erschienen...', [], 'game') . "</p>";
            if ($town->isOpen()){
                $text .= "<p>" . $this->translator->trans('Die Stadt wird erst starten, wenn sie <strong>{population} Bürger hat</strong>.', ['{population}' => $town->getPopulation()], 'game') . "</p>" . "<a class='help-button'>" . "<div class='tooltip help'>" . $this->translator->trans("Falls sich dieser Zustand auch um Mitternacht noch nicht geändert hat, findet kein Zombieangriff statt. Der Tag wird dann künstlich verlängert.", [], 'global') . "</div>" . $this->translator->trans("Hilfe", [], 'global') . "</a>";
            } else {
                $text .= $this->translator->trans('Fangt schon mal an zu beten, Bürger - die Zombies werden um Mitternacht angreifen!', [], 'game');
            }
        } else {
            $gazette_logs = $this->entity_manager->getRepository(GazetteLogEntry::class)->findBy(['gazette' => $gazette]);
            $num = 0;

            $in_between = [
                T::__('Eilmeldung:', 'game'),
                T::__('Liebe Bürgerinnen und Bürger,', 'game'),
                T::__('Neue Nachrichten:', 'game'),
                T::__('Eine kleine Anekdote:', 'game'),
                T::__('Das muss hier auch noch erzählt werden:', 'game'),
                T::__('Folgendes:', 'game'),
                T::__('Das Neueste aus der Gerüchteküche:', 'game'),
                T::__('Die Gerüchteküche brodelt mal wieder:', 'game'),
                T::__('Klatsch und Tratsch:', 'game'),
                T::__('Aktuelles in Kürze:', 'game'),
                T::__('WICHTIG:', 'game'),
                T::__('Hinweis an die Bevölkerung:', 'game'),
                T::__('Zur Aufheiterung:', 'game'),
                T::__('Gut zu wissen:', 'game'),
                T::__('Mal was anderes:', 'game'),
                T::__('Das neueste Geschwätz:', 'game'),
            ];
            $in_between_mod = (ceil(count($in_between)/10) + 1) * 10;

            while (count($gazette_logs) > 0) {
                /** @var GazetteLogEntry $log */
                $log = array_shift($gazette_logs);
                if ($log->getTemplate() === null)
                    continue;
                $num++;
                $type = $log->getTemplate()->getType();
                if ($type !== GazetteEntryTemplate::TypeGazetteWind) {
                    if ($num === 2)
                        $inbetween = $in_between[$log->getId() % $in_between_mod] ?? '';
                    else $inbetween = '';
                    $text .= '<p>' . ($inbetween !== '' ? ($this->translator->trans($inbetween,[],'game') . ' ') : '') . $this->parseGazetteLog($log, $inbetween !== '') . '</p>';
                } else
                    $wind = $this->parseGazetteLog($log);
            }
        }


        $textClass = "day$day";

        $days = [
            'final' => $day % 5,
            'repeat' => floor($day / 5),
        ];

        if($origLang !== $lang) {
            $this->translator->setLocale($origLang);
        }

        return [
            'name' => $town->getName(),
            'open' => $town->isOpen(),
            'day' => $day,
            'days' => $days,
            'devast' => $town->getDevastated(),
            'chaos' => $town->getChaos(),
            'door' => $gazette->getDoor(),
            'reactorExplosion' => $gazette->getReactorExplosion(),
            'death_outside' => $death_outside,
            'death_inside' => $death_inside,
            'attack' => $gazette->getAttack(),
            'defense' => $gazette->getDefense(),
            'invasion' => $gazette->getInvasion(),
            'deaths' => count($death_inside),
            'terror' => $gazette->getTerror(),
            'text' => $text,
            'textClass' => $textClass,
            'wind' => $wind,
            'windDirection' => intval($gazette->getWindDirection()),
            'waterlost' => intval($gazette->getWaterlost()),
        ];
    }
}